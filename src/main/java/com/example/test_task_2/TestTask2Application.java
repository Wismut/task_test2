package com.example.test_task_2;

import com.example.test_task_2.service.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@EnableScheduling
@SpringBootApplication
public class TestTask2Application {
    @Autowired
    private Runner runner;

    public static void main(String[] args) {
        SpringApplication.run(TestTask2Application.class, args);
    }

    @Bean
    public CommandLineRunner run() throws Exception {
        return (args) -> {
            runner.run();
        };
    }

    @Bean(name = "blockingQueue")
    public BlockingQueue<String> blockingQueue() {
        return new LinkedBlockingQueue<>();
    }

    @Scheduled(fixedDelay = 5000)
    public void scheduleFixedDelayTask() {
        runner.displayCompaniesInfo();
    }
}
