package com.example.test_task_2.service;

import com.example.test_task_2.model.CompanyQuote;

public interface CompanyQuoteService {
    CompanyQuote loadByUrl(String url);

    void displayCompanyQuotes();
}
