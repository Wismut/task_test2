package com.example.test_task_2.service;

import com.example.test_task_2.model.Company;

import java.util.List;

public interface CompanyService {
    void putDataOntoQueue(List<Company> companies);

    boolean isEnabled(Company company);

    List<Company> loadCompanies();
}
