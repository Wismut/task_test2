package com.example.test_task_2.service;

import com.example.test_task_2.model.Company;

import java.util.List;

public interface CompanyStorageService {
    void setCompanies(List<Company> companies);

    List<Company> getCompanies();
}
