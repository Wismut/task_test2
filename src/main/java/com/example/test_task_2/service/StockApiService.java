package com.example.test_task_2.service;

public interface StockApiService {
    String buildQuoteApiUrl(String companySymbol);
}
