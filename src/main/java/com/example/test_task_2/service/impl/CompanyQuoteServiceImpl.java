package com.example.test_task_2.service.impl;

import com.example.test_task_2.model.CompanyQuote;
import com.example.test_task_2.service.CompanyQuoteService;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.concurrent.BlockingQueue;

@Service
public class CompanyQuoteServiceImpl implements CompanyQuoteService {
    private final RestTemplate restTemplate = new RestTemplate();

    @Resource(name = "blockingQueue")
    private BlockingQueue<String> queue;

    @Override
    public CompanyQuote loadByUrl(String url) {
        return restTemplate.getForObject(url, CompanyQuote.class);
    }

    @Override
    public void displayCompanyQuotes() {
        try {
            while (!queue.isEmpty()) {
                System.out.println(loadByUrl(queue.take()));
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
