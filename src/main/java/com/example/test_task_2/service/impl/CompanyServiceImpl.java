package com.example.test_task_2.service.impl;

import com.example.test_task_2.model.Company;
import com.example.test_task_2.service.CompanyService;
import com.example.test_task_2.service.StockApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.stream.Collectors;

@Service
public class CompanyServiceImpl implements CompanyService {
    @Resource(name = "blockingQueue")
    private BlockingQueue<String> quoteUrls;

    @Value("${stock.api.url}")
    private String stockApiUrl;

    @Value("${token.api}")
    private String tokenApi;

    private final StockApiService stockApiService;
    private final RestTemplate restTemplate = new RestTemplate();

    @Autowired
    public CompanyServiceImpl(StockApiService stockApiService) {
        this.stockApiService = stockApiService;
    }

    @Override
    public void putDataOntoQueue(List<Company> companies) {
        companies.stream()
                .filter(Company::isEnabled)
                .forEach(c -> quoteUrls.add(stockApiService.buildQuoteApiUrl(c.getSymbol())));
        System.out.println("quoteUrls size: " + quoteUrls.size());
    }

    @Override
    public boolean isEnabled(Company company) {
        return company.isEnabled();
    }

    @Override
    public List<Company> loadCompanies() {
        Company[] companies = restTemplate.getForObject(stockApiUrl + tokenApi,
                Company[].class);
        Objects.requireNonNull(companies, "Companies have not loaded");
        return Arrays.stream(companies)
                .collect(Collectors.toList());
    }
}
