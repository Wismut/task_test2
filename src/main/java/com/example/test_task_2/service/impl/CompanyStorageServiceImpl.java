package com.example.test_task_2.service.impl;

import com.example.test_task_2.model.Company;
import com.example.test_task_2.service.CompanyStorageService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CompanyStorageServiceImpl implements CompanyStorageService {
    private List<Company> companies = new ArrayList<>();

    @Override
    public List<Company> getCompanies() {
        return companies;
    }

    @Override
    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
}
