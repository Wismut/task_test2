package com.example.test_task_2.service.impl;

import com.example.test_task_2.model.Company;
import com.example.test_task_2.service.CompanyQuoteService;
import com.example.test_task_2.service.CompanyService;
import com.example.test_task_2.service.CompanyStorageService;
import com.example.test_task_2.service.Runner;
import org.springframework.stereotype.Service;

@Service
public class RunnerImpl implements Runner {
    private final CompanyService companyService;
    private final CompanyStorageService companyStorageService;
    private final CompanyQuoteService companyQuoteService;

    public RunnerImpl(CompanyService companyService, CompanyStorageService companyStorageService, CompanyQuoteService companyQuoteService) {
        this.companyService = companyService;
        this.companyStorageService = companyStorageService;
        this.companyQuoteService = companyQuoteService;
    }

    @Override
    public void run() {
        companyStorageService.setCompanies(companyService.loadCompanies());
        companyService.putDataOntoQueue(companyStorageService.getCompanies());
        companyQuoteService.displayCompanyQuotes();
    }

    @Override
    public void displayCompaniesInfo() {
        for (Company company : companyStorageService.getCompanies()) {
//            System.out.println(company);
        }
    }
}
