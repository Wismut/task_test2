package com.example.test_task_2.service.impl;

import com.example.test_task_2.service.StockApiService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class StockApiServiceImpl implements StockApiService {
    @Value("${company.api.url}")
    private String companyApiUrl;

    @Value("${company.api.params.url}")
    private String companyApiParamsUrl;

    @Value("${token.api}")
    private String tokenApi;

    @Override
    public String buildQuoteApiUrl(String companySymbol) {
        return new StringBuilder()
                .append(companyApiUrl)
                .append(companySymbol)
                .append(companyApiParamsUrl)
                .append(tokenApi)
                .toString();
    }
}
